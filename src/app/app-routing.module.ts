import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageModule } from './views/home/home.module';

const routes: Routes = [
 { path: 'edit', loadChildren: './views/edit/edit.module#EditPageModule' },
 { path: 'list', loadChildren: './views/list/list.module#ListPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),HomePageModule],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
