import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { RestApiService } from 'src/app/rest-api.service';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  public items: Array<{ title: string; note: string; icon: string }> = [];
  apis: any;
  constructor(public api: RestApiService, public loadingController: LoadingController) {
   console.log("hello");
  }

	async getAllapis() {
	  const loading = await this.loadingController.create({
	
	  });
	  await loading.present();
	  await this.api.getAllapis()
		.subscribe(res => {
		  console.log(res);
		  this.apis = res;
		  loading.dismiss();
		}, err => {
		  console.log(err);
		  loading.dismiss();
		});
	}

  ngOnInit() {
	  this.getAllapis();
  }
   // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
