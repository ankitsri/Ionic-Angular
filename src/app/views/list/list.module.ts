import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule, Routes } from '@angular/router';

import { ListPage } from './list.page';

const routes: Routes = [
 
  {
    path: '',
    component: ListPage
  },{
    path: '1',
    component: ListPage
  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(
      routes
    )
  ],
  declarations: [ListPage]
})
export class ListPageModule {
  
}
